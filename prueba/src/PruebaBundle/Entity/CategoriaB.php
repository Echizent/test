<?php

namespace PruebaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriaB
 *
 * @ORM\Table(name="categoria_b", indexes={@ORM\Index(name="catbxcata", columns={"fk_categoriaA"})})
 * @ORM\Entity
 */
class CategoriaB
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoriaB", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCategoriab;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCatB", type="string", length=50, nullable=false)
     */
    private $nombrecatb;

    /**
     * @var string
     *
     * @ORM\Column(name="identificadorCatB", type="string", length=50, nullable=false)
     */
    private $identificadorcatb;

    /**
     * @var \CategoriaA
     *
     * @ORM\ManyToOne(targetEntity="CategoriaA", inversedBy="categorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_categoriaA", referencedColumnName="id_categoriaA")
     * })
     */
    private $fkCategoriaa;

    /**
    * @ORM\OneToMany(targetEntity="Producto", mappedBy="fkCategoriab")
    */
    private $productos;

    public function __construct()
    {
        $this->productos = new ArrayCollection();
    }
    /**
     * Get idCategoriab
     *
     * @return integer
     */
    public function getIdCategoriab()
    {
        return $this->idCategoriab;
    }

    /**
     * Set nombrecatb
     *
     * @param string $nombrecatb
     *
     * @return CategoriaB
     */
    public function setNombrecatb($nombrecatb)
    {
        $this->nombrecatb = $nombrecatb;

        return $this;
    }

    /**
     * Get nombrecatb
     *
     * @return string
     */
    public function getNombrecatb()
    {
        return $this->nombrecatb;
    }

    /**
     * Set identificadorcatb
     *
     * @param string $identificadorcatb
     *
     * @return CategoriaB
     */
    public function setIdentificadorcatb($identificadorcatb)
    {
        $this->identificadorcatb = $identificadorcatb;

        return $this;
    }

    /**
     * Get identificadorcatb
     *
     * @return string
     */
    public function getIdentificadorcatb()
    {
        return $this->identificadorcatb;
    }

    /**
     * Set fkCategoriaa
     *
     * @param \PruebaBundle\Entity\CategoriaA $fkCategoriaa
     *
     * @return CategoriaB
     */
    public function setFkCategoriaa(\PruebaBundle\Entity\CategoriaA $fkCategoriaa = null)
    {
        $this->fkCategoriaa = $fkCategoriaa;

        return $this;
    }

    /**
     * Get fkCategoriaa
     *
     * @return \PruebaBundle\Entity\CategoriaA
     */
    public function getFkCategoriaa()
    {
        return $this->fkCategoriaa;
    }

    public function __toString(){
      return $this->nombrecatb;
    }

    /**
     * Add producto
     *
     * @param \PruebaBundle\Entity\Producto $producto
     *
     * @return CategoriaB
     */
    public function addProducto(\PruebaBundle\Entity\Producto $producto)
    {
        $this->productos[] = $producto;

        return $this;
    }

    /**
     * Remove producto
     *
     * @param \PruebaBundle\Entity\Producto $producto
     */
    public function removeProducto(\PruebaBundle\Entity\Producto $producto)
    {
        $this->productos->removeElement($producto);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductos()
    {
        return $this->productos;
    }
}

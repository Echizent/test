<?php

namespace PruebaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriaA
 *
 * @ORM\Table(name="categoria_a")
 * @ORM\Entity
 */
class CategoriaA
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoriaA", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCategoriaa;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCatA", type="string", length=50, nullable=false)
     */
    private $nombrecata;

    /**
     * @var string
     *
     * @ORM\Column(name="identificadorCatA", type="string", length=50, nullable=false)
     */
    private $identificadorcata;

    /**
     * @ORM\OneToMany(targetEntity="CategoriaB", mappedBy="categorias")
     */
    private $categorias;

    public function __construct()
    {
        $this->categorias = new ArrayCollection();
    }

    /**
     * Get idCategoriaa
     *
     * @return integer
     */
    public function getIdCategoriaa()
    {
        return $this->idCategoriaa;
    }

    /**
     * Set nombrecata
     *
     * @param string $nombrecata
     *
     * @return CategoriaA
     */
    public function setNombrecata($nombrecata)
    {
        $this->nombrecata = $nombrecata;

        return $this;
    }

    /**
     * Get nombrecata
     *
     * @return string
     */
    public function getNombrecata()
    {
        return $this->nombrecata;
    }

    /**
     * Set identificadorcata
     *
     * @param string $identificadorcata
     *
     * @return CategoriaA
     */
    public function setIdentificadorcata($identificadorcata)
    {
        $this->identificadorcata = $identificadorcata;

        return $this;
    }

    /**
     * Get identificadorcata
     *
     * @return string
     */
    public function getIdentificadorcata()
    {
        return $this->identificadorcata;
    }

    public function __toString(){
      return $this->nombrecata;
    }

    /**
     * Add categoria
     *
     * @param \PruebaBundle\Entity\CategoriaB $categoria
     *
     * @return CategoriaA
     */
    public function addCategoria(\PruebaBundle\Entity\CategoriaB $categoria)
    {
        $this->categorias[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \PruebaBundle\Entity\CategoriaB $categoria
     */
    public function removeCategoria(\PruebaBundle\Entity\CategoriaB $categoria)
    {
        $this->categorias->removeElement($categoria);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }
}

<?php

namespace PruebaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Producto
 *
 * @ORM\Table(name="producto", indexes={@ORM\Index(name="prodxcatb", columns={"fk_categoriaB"})})
 * @ORM\Entity
 */
class Producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_producto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProducto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50, nullable=false)
     */
    private $descripcion;

    /**
     * @var \CategoriaB
     *
     * @ORM\ManyToOne(targetEntity="CategoriaB", inversedBy="productos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_categoriaB", referencedColumnName="id_categoriaB")
     * })
     */
    private $fkCategoriab;



    /**
     * Get idProducto
     *
     * @return integer
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fkCategoriab
     *
     * @param \PruebaBundle\Entity\CategoriaB $fkCategoriab
     *
     * @return Producto
     */
    public function setFkCategoriab(\PruebaBundle\Entity\CategoriaB $fkCategoriab = null)
    {
        $this->fkCategoriab = $fkCategoriab;

        return $this;
    }

    /**
     * Get fkCategoriab
     *
     * @return \PruebaBundle\Entity\CategoriaB
     */
    public function getFkCategoriab()
    {
        return $this->fkCategoriab;
    }

    public function __toString(){
      return $this->nombre;
    }
}

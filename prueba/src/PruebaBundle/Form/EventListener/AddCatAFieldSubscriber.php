<?php
  namespace PruebaBundle\Form\EventListener;

  use Symfony\Component\Form\FormEvent;
  use Symfony\Component\Form\FormEvents;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;
  use Symfony\Component\PropertyAccess\PropertyAccess;
  use Symfony\Bridge\Doctrine\Form\Type\EntityType;


  /**
   *
   */
  class AddCatAFieldSubscriber implements EventSubscriberInterface{
    private $propertyPathToCategoriaB;

    public function __construct($propertyPathToCategoriaB){
      $this->propertyPathToCategoriaB = $propertyPathToCategoriaB;
    }

    public static function getSubscribedEvents(){
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit'
        );
    }

    private function addCatAForm($form, $categoriaA = null){
      $formOptions = array(
        'class' => 'PruebaBundle:CategoriaA',
        'mapped' => false,
        'label' => 'Categoria A',
        'placeholder' => 'Categoria A',
        'attr' => array(
          'class' => 'class_select_categoriaA',
        ),
      );

      if($categoriaA){
        $formOptions['data'] = $categoriaA;
      }

      $form->add('categoriaA', EntityType::class, $formOptions);
    }

    public function preSetData(FormEvent $event){
      $data = $event->getData();
      $form = $event->getForm();

      if(null === $data){
        return;
      }

      $accessor = PropertyAccess::createPropertyAccessor();

      $catB = $accessor->getValue($data, $this->propertyPathToCategoriaB);
      $catA = ($catB) ? $catB->getFkCategoriaa() : null;

      $this->addCatAForm($form, $catA);
    }

    public function preSubmit(FormEvent $event){
      $form = $event->getForm();

      $this->addCatAForm($form);
    }

  }

?>

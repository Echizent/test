<?php
  namespace PruebaBundle\Form\EventListener;

  use Symfony\Component\Form\FormEvent;
  use Symfony\Component\Form\FormEvents;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;
  use Symfony\Component\PropertyAccess\PropertyAccess;
  use Doctrine\ORM\EntityRepository;
  use Symfony\Bridge\Doctrine\Form\Type\EntityType;


  /**
   *
   */
  class AddCatBFieldSubscriber implements EventSubscriberInterface{
    private $propertyPathToCategoriaB;

    public function __construct($propertyPathToCategoriaB){
      $this->propertyPathToCategoriaB = $propertyPathToCategoriaB;
    }

    public static function getSubscribedEvents(){
       return array(
           FormEvents::PRE_SET_DATA => 'preSetData',
           FormEvents::PRE_SUBMIT => 'preSubmit'
       );
   }

   private function addCatBForm($form, $catAid){
     $formOptions = array(
       'class' => 'PruebaBundle:CategoriaB',
       'placeholder' => 'Categoria B',
       'label' => 'Categoria B',
       //'multiple' => true,
       'attr' => array(
         'class' => 'class_select_categoriaB',
       ),
       'query_builder' => function (EntityRepository $repository) use ($catAid){
         $qb = $repository->createQueryBuilder('catb')
         ->innerJoin('catb.fkCategoriaa', 'fkCategoriaa')
         ->where('fkCategoriaa.idCategoriaa = :fkCategoriaa')
         ->setParameter('fkCategoriaa',$catAid);
         //var_dump($qb);
         return $qb;
         }
     );
     $form->add($this->propertyPathToCategoriaB, EntityType::class, $formOptions);
    }

    public function preSetData(FormEvent $event){
      $data = $event->getData();
      $form = $event->getForm();

      if(null === $data)
      {
        return;
      }

      $accessor = PropertyAccess::createPropertyAccessor();

      $catB = $accessor->getValue($data, $this->propertyPathToCategoriaB);
      $catA = ($catB) ? $catB->getIdCategoriab->getFkCategoriaa()->getIdCategoriaa : null;

      $this->addCatBForm($form, $catA);

    }

    public function preSubmit(FormEvent $event){
      $data = $event->getData();
      $form = $event->getForm();

      $catA = array_key_exists('categoriaA',$data) ? $data['categoriaA'] : null;

      $this->addCatBForm($form,$catA);

    }


  }

?>

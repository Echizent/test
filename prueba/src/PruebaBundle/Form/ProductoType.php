<?php

namespace PruebaBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use PruebaBundle\Form\EventListener\AddCatAFieldSubscriber;
use PruebaBundle\Form\EventListener\AddCatBFieldSubscriber;

class ProductoType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
        ->add('nombre',TextType::class)
        ->add('descripcion',TextType::class)
        /*->add('categoriaA', EntityType::class, array(
          'class' => 'PruebaBundle:CategoriaA',
          'mapped' => false,
          'label' => 'Categoria A',
          'placeholder' => 'Elegir Cat A',
          'attr' => array(
            'class' => 'class_select_categoriaA',
        )))*/
        /*->add('fkCategoriab', EntityType::class, array(
          'class' => 'PruebaBundle:CategoriaB',
          'placeholder' =>false,
        ))*/
        ->add('crear', SubmitType::class);

        $builder->addEventSubscriber(new AddCatAFieldSubscriber('fkCategoriab'));
        $builder->addEventSubscriber(new AddCatBFieldSubscriber('fkCategoriab'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'PruebaBundle\Entity\Producto'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(){
        return 'pruebabundle_producto';
    }
}

<?php

namespace PruebaBundle\Controller;

use PruebaBundle\Entity\Producto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PruebaBundle\Entity\CategoriaBRepository;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller
{

  /**
   * @Route("/fkCategoriab", name="select_catB")
   */
 public function fkCategoriabAction(Request $request)
 {
     $catA_id = $request->request->get('catA_id');

     $em = $this->getDoctrine()->getManager();
     //$catB = $em->getRepository('PruebaBundle:CategoriaB')->findAll();
     $catB = $em->getRepository('PruebaBundle:CategoriaB')->findByfkCategoriaa($catA_id);
     for ($i=0; $i < count($catB) ; $i++) {
       $catB2[$i]['idCategoriab'] = $catB[$i]->getIdCategoriab();
       $catB2[$i]['nombreCatB'] = $catB[$i]->getNombrecatb();
     }
     $catTest = array(array('idCategoriab' => '1','nombreCatB' => 'ROCAS'));
     //return $catB;
     return new JsonResponse($catB2);
 }
   /**
    * Lists all producto entities.
    *
    */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $productos = $em->getRepository('PruebaBundle:Producto')->findAll();

        return $this->render('producto/index.html.twig', array(
            'productos' => $productos,
        ));
    }

    /**
     * Creates a new producto entity.
     *
     */
    public function newAction(Request $request)
    {
        $producto = new Producto();
        /*var_dump($request->request->all());*/

        /**/echo "</br>---------DESPUES DEL FORM------------</br>";/**/

        $form = $this->createForm('PruebaBundle\Form\ProductoType', $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //echo "</br> IDCATA = ".$request->request->get("pruebabundle_producto")["Test"][0]."</br>";
          //  var_dump($request->request->all()["pruebabundle_producto"]["Test"]);
            $em = $this->getDoctrine()->getManager();
            $em->persist($producto);
            $em->flush();

            //return $this->render('PruebaBundle:Default:index.html.twig');
            return $this->redirectToRoute('producto_show', array('idProducto' => $producto->getIdproducto()));
        }

        return $this->render('producto/new.html.twig', array(
            'producto' => $producto,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a producto entity.
     *
     */
    public function showAction(Producto $producto)
    {
        $deleteForm = $this->createDeleteForm($producto);

        return $this->render('producto/show.html.twig', array(
            'producto' => $producto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing producto entity.
     *
     */
    public function editAction(Request $request, Producto $producto)
    {
        $deleteForm = $this->createDeleteForm($producto);
        $editForm = $this->createForm('PruebaBundle\Form\ProductoType', $producto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('producto_edit', array('idProducto' => $producto->getIdproducto()));
        }

        return $this->render('producto/edit.html.twig', array(
            'producto' => $producto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a producto entity.
     *
     */
    public function deleteAction(Request $request, Producto $producto)
    {
        $form = $this->createDeleteForm($producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($producto);
            $em->flush();
        }

        return $this->redirectToRoute('producto_index');
    }

    /**
     * Creates a form to delete a producto entity.
     *
     * @param Producto $producto The producto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Producto $producto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('producto_delete', array('idProducto' => $producto->getIdproducto())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

<?php
  namespace PruebaBundle\Controller;

  use PruebaBundle\Entity\Producto;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\JsonResponse;

  /**
   *
   */
  class CategoriaBController extends Controller
  {

    public function fkCategoriabAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $catA = $request->request->get('idCategoriaa');
      $catB = $em->getRepository('PruebaBundle:CategoriaB')->findByfkCategoriaa($catA);
      return new JsonResponse($catB);
    }
  }

?>
